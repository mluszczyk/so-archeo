#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "common.h"

// those things don't change after main loop starts
int depth = 0;
int length = 0;
long fixed_fee = -1;
int max_artefact = -1;

int museum_queue_id;

bool running = true;

// type describing a single field
typedef struct {
    long owner;
    // number of reserved levels, default: 0
    int reserved_depth;
    // lowest excavated level, default: -1 (then 0, 1..)
    int excavated_depth;
    bool is_being_excavated;
    int *estimates;
    int *treasure;
} field_t;

// globals variables that are modified and read by multiple threads
// are encapsulated into a struct, so as to easily figure out when
// mutex should be used.
struct {
    char *reports[MAX_COMPANY_NUM];
    // number of fields on which the company can work, i.e. number of
    // reserved fields minus number of exploited fields that belong to it
    int available_fields[MAX_COMPANY_NUM];
    field_t *fields;
} museum = {{NULL}, {0}, NULL};

struct {
    bool alive;
    pthread_t thread;
} delegates[MAX_COMPANY_NUM];

pthread_mutex_t mutex;  // protects everything inside struct museum

static void sigint_handler(int signal) {
    int bank_queue_id = msgget(BANK_KEY, 0700 | IPC_CREAT);

    bank_msg_t bank_msg;
    bank_msg.type = BANK_TYPE;
    bank_msg.data.action = MUSEUM_SHUTDOWN;

    int sndret = msgsnd(bank_queue_id, &bank_msg, sizeof(bank_msg.data), 0);
    SYSCHK(sndret == 0);
}

static void set_sigint_handler() {
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, NULL);
}

static void accept_report(int sender, char *data) {
    assert(museum.reports[sender] == NULL);

    pthread_mutex_lock(&mutex);
    museum.reports[sender] = strdup(data);

    for (int i = 0; i < length; ++i) {
        if (museum.fields[i].owner == sender) {
            museum.fields[i].owner = 0;
            museum.fields[i].reserved_depth = 0;
        }
    }
    pthread_mutex_unlock(&mutex);
}

static void reject_enquiry(int company_id, long long offer) {
    int bank_queue_id = msgget(BANK_KEY, 0700 | IPC_CREAT);

    bank_msg_t bank_msg;
    bank_msg.type = company_id;
    bank_msg.data.action = ENQUIRY_REJECTION;
    bank_msg.data.u.enquiry_rejection.offer = offer;

    int sndret = msgsnd(bank_queue_id, &bank_msg, sizeof(bank_msg.data), 0);
    SYSCHK(sndret == 0);
}

// used inside a mutex
int get_depth(int begin, int size, long long bid) {
    for (int i = begin; i < begin + size; ++i) {
        if (museum.fields[i].reserved_depth != 0) {
            return -1;
        }
    }

    int best_depth = -1;
    long long cur_estimate = 0;
    for (int d = 0; d < depth && cur_estimate < bid; ++d) {
        for (int i = begin; i < begin + size; ++i) {
            cur_estimate += museum.fields[i].estimates[d];
        }

        if (cur_estimate > 0 && cur_estimate <= bid) {
            best_depth = d;
        }
    }

    return best_depth;
}

// assumes that mutex is locked
static void accept_enquiry(int company_id, int begin, int workers_num,
                           int depth, long long bid) {
    for (int i = begin; i < begin + workers_num; ++i) {
        museum.fields[i].reserved_depth = depth + 1;
        museum.fields[i].owner = company_id;
    }
}

static void send_enquiry_acceptation(int company_id, int begin,
                                     int depth, long long offer) {
    int bank_queue_id = msgget(BANK_KEY, 0700 | IPC_CREAT);
    SYSCHK(bank_queue_id >= 0);

    bank_msg_t bank_msg;
    bank_msg.type = company_id;
    bank_msg.data.action = ENQUIRY_ACCEPTATION;
    bank_msg.data.u.enquiry_acceptation.offer = offer;
    bank_msg.data.u.enquiry_acceptation.start = begin;
    bank_msg.data.u.enquiry_acceptation.depth = depth;
    bank_msg.data.u.enquiry_acceptation.delegate = company_id;

    int sndret = msgsnd(bank_queue_id, &bank_msg, sizeof(bank_msg.data), 0);
    SYSCHK(sndret == 0);
}

// send given number representing buried treasure to the company
static void send_treasure(long worker, int field, int depth) {
    int bank_queue_id = msgget(BANK_KEY, 0700);
    SYSCHK(bank_queue_id >= 0);

    int treasure = museum.fields[field].treasure[depth];
    assert(worker > 0);
    bank_msg_t bank_msg;
    bank_msg.type = worker;
    bank_msg.data.action = TREASURE;
    bank_msg.data.u.treasure = treasure;
    LOG("Send terrain (%d, %d, %d) to worker %ld\n",
        field, depth, treasure, worker);

    int sndret = msgsnd(bank_queue_id, &bank_msg, sizeof(bank_msg.data), 0);
    SYSCHK(sndret == 0);
}
// inform that access to specified level of given field is prohibited
static void reject_terrain_request(long worker) {
    int bank_queue_id = msgget(BANK_KEY, 0700);
    SYSCHK(bank_queue_id >= 0);

    assert(worker > 0);
    bank_msg_t bank_msg;
    bank_msg.type = worker;
    bank_msg.data.action = TERRAIN_REJECTION;
    LOG("Reject terrain information %ld\n", worker);

    int sndret = msgsnd(bank_queue_id, &bank_msg, sizeof(bank_msg.data), 0);
    SYSCHK(sndret == 0);
}

// removes all permissions belonging to a given company
// must be called inside a mutex
// should be called after all company's fields are exploited
static void clean_permissions(long company_id) {
    LOG("Freeing up all permissions granted to %ld\n", company_id);
    for (int i = 0; i < length; ++i) {
        if (museum.fields[i].owner == company_id) {
            museum.fields[i].owner = 0;
            museum.fields[i].reserved_depth = 0;
        }
    }
}

// takes artefacts from the company
static void handle_message_artefacts(int field, long company_id) {
    pthread_mutex_lock(&mutex);
    int depth = ++museum.fields[field].excavated_depth;
    museum.fields[field].estimates[depth] = 0;
    museum.fields[field].treasure[depth] = 1;
    museum.fields[field].is_being_excavated = false;
    if (museum.fields[field].excavated_depth + 1 ==
            museum.fields[field].reserved_depth) {
        --museum.available_fields[company_id];
        if (museum.available_fields[company_id]) {
            clean_permissions(company_id);
        }
    }
    pthread_mutex_unlock(&mutex);
}

// delegate thread
static void* delegate_main(void *arg) {
    long company_id = (long)arg;

    while (running) {
        museum_msg_t msg;
        int rcvret = msgrcv(museum_queue_id, &msg, sizeof(msg.data),
                            company_id, 0);
        if (rcvret == -1 && errno == EINTR) {
            continue;
        }
        if (rcvret == -1 && errno == EIDRM) {
            // the museum is being shut down
            break;
        }

        SYSCHK(rcvret > 0);

        switch (msg.data.action) {
        case TREASURE_REQUEST:
            pthread_mutex_lock(&mutex);
            int field = msg.data.u.treasure_request.field;
            int depth = museum.fields[field].excavated_depth + 1;
            if (museum.fields[field].is_being_excavated ||
                    depth >= museum.fields[field].reserved_depth) {
                pthread_mutex_unlock(&mutex);
                reject_terrain_request(msg.data.u.treasure_request.worker);
            } else {
                museum.fields[field].is_being_excavated = true;
                pthread_mutex_unlock(&mutex);
                send_treasure(msg.data.u.treasure_request.worker,
                              field, depth);
            }
            break;
        case ARTEFACTS:
            handle_message_artefacts(msg.data.u.artefacts.field,
                                     msg.data.u.artefacts.sender);
            break;
        default:
            assert(false);
        }
    }

    pthread_mutex_lock(&mutex);
    delegates[company_id].alive = false;
    pthread_mutex_unlock(&mutex);

    return NULL;
}

static void create_delegate(long company_id) {
    pthread_create(&delegates[company_id].thread, NULL,
                   &delegate_main, (void *)company_id);
}

static void respond_to_enquiry(long company_id, long long bid,
                               int workers_num) {
    LOG("Received offer %lld from %ld\n", bid, company_id);
    pthread_mutex_lock(&mutex);

    if (!delegates[company_id].alive) {
        for (int i = 0; i < length - workers_num; ++i) {
            int depth = get_depth(i, workers_num, bid);
            if (depth > 0) {
                accept_enquiry(company_id, i, workers_num, depth, bid);
                delegates[company_id].alive = true;
                pthread_mutex_unlock(&mutex);
                create_delegate(company_id);
                send_enquiry_acceptation(company_id, i, depth, bid);
                return;
            }
        }
    }

    pthread_mutex_unlock(&mutex);
    reject_enquiry(company_id, bid);
}

void main_loop() {
    while (running) {
        museum_msg_t museum_msg;
        int rcvret = msgrcv(museum_queue_id, &museum_msg,
                            sizeof(museum_data_t),
                            MUSEUM_TYPE, 0);
        if (rcvret == -1 && errno == EINTR) {
            continue;  // it's OK, try again
        }
        SYSCHK(rcvret > 0);

        switch (museum_msg.data.action) {
        case BANK_SHUTDOWN:
            running = false;
            break;
        case COMPANY_REPORT:
            accept_report(museum_msg.data.u.company_report.sender,
                          museum_msg.data.u.company_report.report);
            break;
        case PERMISSION_ENQUIRY:
            assert(museum_msg.data.u.permission_enquiry.sender != 0);
            respond_to_enquiry(museum_msg.data.u.permission_enquiry.sender,
                               museum_msg.data.u.permission_enquiry.offer,
                               museum_msg.data.u.permission_enquiry
                                .workers_num);
            break;
        case COLLECTION_TRANSFER:
            // doesn't do anything
            break;
        default:
            assert(false);
        }
    }
}

int main(int argc, char *argv[]) {
    assert(argc == 5);

    sscanf(argv[1], "%d", &length);
    sscanf(argv[2], "%d", &depth);
    sscanf(argv[3], "%ld", &fixed_fee);
    sscanf(argv[4], "%d", &max_artefact);

    museum.fields = calloc(length, sizeof(field_t));
    memset(museum.fields, 0, sizeof(field_t) * length);

    for (int i = 0; i < length; ++i) {
        museum.fields[i].excavated_depth= -1;
    }

    for (int i = 0; i < length; ++i) {
        museum.fields[i].estimates = calloc(depth, sizeof(int));
        museum.fields[i].treasure = calloc(depth, sizeof(int));
    }

    for (int i = 0; i < length; ++i) {
        for (int j = 0; j < depth; ++j) {
            scanf("%d", &museum.fields[i].treasure[j]);
        }
    }

    for (int i = 0; i < length; ++i) {
        for (int j = 0; j < depth; ++j) {
            scanf("%d", &museum.fields[i].estimates[j]);
        }
    }


    set_sigint_handler();
    pthread_mutex_init(&mutex, NULL);

    museum_queue_id = msgget(MUSEUM_KEY, 0700 | IPC_CREAT);

    main_loop();

    SYSCHK(msgctl(museum_queue_id, IPC_RMID, NULL) == 0);

    for (int i = 0; i < MAX_COMPANY_NUM; ++i) {
        if (delegates[i].alive) {
            pthread_join(delegates[i].thread, NULL);
        }
    }
    pthread_mutex_destroy(&mutex);

    for (int i = 0; i < length; ++i) {
        for (int j = 0; j < depth; ++j) {
            if (museum.fields[i].excavated_depth >= j)
                printf("0");
            else
                printf("2");
        }
        printf("\n");
    }

    for (int i = 0; i < MAX_COMPANY_NUM; ++i) {
        if (museum.reports[i] != NULL) {
            printf("\n%s\n", museum.reports[i]);
            free(museum.reports[i]);
        }
    }

    for (int i = 0; i < length; ++i) {
        free(museum.fields[i].estimates);
        free(museum.fields[i].treasure);
    }
    free(museum.fields);

    return 0;
}
