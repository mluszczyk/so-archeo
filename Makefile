CFLAGS=-std=gnu99 -lrt -lpthread
# powyżej można dodać -DLOGGING
all: bank muzeum

muzeum: muzeum.c common.o
	cc ${CFLAGS} muzeum.c common.o -o muzeum

bank: company.o bank.c common.o
	cc ${CFLAGS} bank.c company.o common.o -o bank
