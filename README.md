Niestety, nie wszystko jest zaimplementowane.

Trochę informacji do debugowania pojawia się po kompilacji -DLOGGING w
Makefile.

Przykładowe użycie zaprezentowane jest w skrypcie run.sh. W razie problemów,
można wyczyścić kolejki przy pomocy skryptu cleanup.sh.

Proszę używać trybu 64-bitowego, ze względu na wysokie typy wiadomości
odbieranych przez pracowników na kolejce IPC.
