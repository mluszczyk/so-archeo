#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "common.h"
#include "company.h"

struct Company {
    int id;
    long long balance;
    int workers_num;
    pid_t pid;
};

struct Company companies[MAX_COMPANY_NUM];
int company_num = 0;
long long fixed_fee;
int max_artefact;

static void shut_down_companies() {
    for (int i = 0; i < company_num; ++i) {
        if (companies[i].pid) {
            kill(companies[i].pid, SIGINT);
        }
    }
}

void sigint_handler(int signal) {
    LOG("Caught SIGINT by the bank %d.\n", getpid());
    shut_down_companies();
}

void set_sigint_handler() {
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, NULL);
}

void spawn_company(int num) {
    pid_t child_pid = fork();
    SYSCHK(child_pid >= 0);

    if (child_pid == 0) {
        company_main(companies[num].id, companies[num].balance,
                     companies[num].workers_num, fixed_fee,
                     max_artefact);
        exit(0);
    } else {
        companies[num].pid = child_pid;
    }
}

static void shut_down_museum() {
    int queue_id = msgget(MUSEUM_KEY, 0700 | IPC_CREAT);
    museum_msg_t museum_msg;
    museum_msg.type = MUSEUM_TYPE;
    museum_msg.data.action = BANK_SHUTDOWN;

    int sndret = msgsnd(queue_id, &museum_msg,
                        sizeof(museum_msg.data), 0);
    SYSCHK(sndret == 0);
}

int main(int argc, char** argv) {
    assert(argc == 4);
    sscanf(argv[1], "%d", &company_num);
    sscanf(argv[2], "%lld", &fixed_fee);
    sscanf(argv[3], "%d", &max_artefact);

    for (int i = 0; i < company_num; ++i) {
        int company_id;
        long long balance;
        int workers_num;

        scanf("%d%lld%d", &company_id, &balance, &workers_num);

        companies[i].id = company_id;
        companies[i].balance = balance;
        companies[i].workers_num = workers_num;
        companies[i].pid = 0;
    }

    int queue_id = msgget(BANK_KEY, 0700 | IPC_CREAT);
    SYSCHK(queue_id >= 0);

    for (int i = 0; i < company_num; ++i) {
        spawn_company(i);
    }

    set_sigint_handler();

    while (company_num > 0) {
        bank_msg_t bank_msg;
        int rcvret = msgrcv(queue_id, &bank_msg, sizeof(bank_data_t),
                            BANK_TYPE, 0);
        if (rcvret == -1 && errno == EINTR) {
            continue;  // signal is expected here
        }
        SYSCHK(rcvret > 0);

        switch (bank_msg.data.action) {
        case COMPANY_SHUTDOWN:
            company_num--;
            break;
        case MUSEUM_SHUTDOWN:
            shut_down_companies();
            break;
        default:
            assert(false);
        }
    }

    LOG("Quitting the bank.\n");

    for (int i = 0; i < company_num; ++i) {
        if (i != 0) {
            SYSCHK(waitpid(i, NULL, 0) >= 0);
        }
    }

    shut_down_museum();
    SYSCHK(msgctl(queue_id, IPC_RMID, NULL) == 0);
}
