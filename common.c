#include <limits.h>

#include "common.h"

const int SLEEP_USECONDS = 1000000;
const key_t BANK_KEY = 732;
const long BANK_TYPE = LONG_MAX;

const key_t MUSEUM_KEY = 724;
const long MUSEUM_TYPE = LONG_MAX;
