#include "company.h"

#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"

bool running = true;
long company_id = -1;
long long company_balance = 0;
int company_workers_num = 0;
long long company_fixed_fee = -1;
int company_max_artefact;  // global artefact limit (A)

struct {
    int depth;
    int start;
    int active_fields;  // number of fields, on which employees can work
} permission = {0, -1, 0};

// always false unless variable has default values (then true or false)
bool asked_for_permission = false;
long delegate = -1;

// worker
typedef enum {
    ASK_FOR_TREASURE,
    WAIT_FOR_TREASURE,
    SEND_ARTEFACTS,
    DONE  // if terrain request was rejected (collected everything)
} worker_state_t;

pthread_mutex_t mutex;
struct {
    pthread_t thread;
    pthread_cond_t condition;
    bool has_work;
} *workers;

int *confirmed_artefacts;
// number of artifcat types with ready collections
int collection_kinds_num = 0;

int bank_queue_id;
int museum_queue_id;

static void quit() {
    bank_msg_t bank_msg;
    bank_msg.type = BANK_TYPE;
    bank_msg.data.action = COMPANY_SHUTDOWN;
    
    int sndret = msgsnd(bank_queue_id, &bank_msg, sizeof(bank_msg.data), 0);
    SYSCHK(sndret == 0);
}

static void sigint_handler(int signal) {
    running = false;
}

static void set_sigint_handler() {
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, NULL);
}

void report() {
    // TODO: should be sent in chunks
    // TODO: more content
    museum_msg_t museum_msg;
    museum_msg.type = MUSEUM_TYPE;
    museum_msg.data.action = COMPANY_REPORT;

    museum_msg.data.u.company_report.sender = company_id;
    memset(museum_msg.data.u.company_report.report, 0,
           sizeof(museum_msg.data.u.company_report.report));
    snprintf(museum_msg.data.u.company_report.report,
             sizeof(museum_msg.data.u.company_report.report),
             "%ld %lld", company_id, company_balance);
    for (int i = 1; i <= company_max_artefact; ++i) {
        if (confirmed_artefacts[i]) {
            char BUFF[100];
            snprintf(BUFF, 100, "\n%d %d", i, confirmed_artefacts[i]);
            strcat(museum_msg.data.u.company_report.report, BUFF);
        }
    }

    bool sent = false;
    while (!sent) {
        int sndret = msgsnd(museum_queue_id, &museum_msg,
                            sizeof(museum_msg.data), 0);
        if (sndret == 0) {
            sent = true;
        } else {
            SYSCHK(errno == EINTR);
        }
    }
}

// Return false, if interrupted
bool ask_for_permission(long long offer) {
    museum_msg_t museum_msg;
    museum_msg.type = MUSEUM_TYPE;
    museum_msg.data.action = PERMISSION_ENQUIRY;
    museum_msg.data.u.permission_enquiry.offer = offer;
    museum_msg.data.u.permission_enquiry.workers_num = company_workers_num;
    museum_msg.data.u.permission_enquiry.sender = company_id;

    int sndret = msgsnd(museum_queue_id, &museum_msg,
                        sizeof(museum_msg.data), 0);

    if (sndret == 0) {
        return true;
    } else {
        SYSCHK(errno == EINTR);
        return false;
    }
}

// Return false, if interrupted. Asks the museum for the number representing
// treasure accessible on given field.
bool ask_for_treasure(long worker_num, int field) {
    museum_msg_t msg;
    assert(delegate > 0);
    msg.type = delegate;
    msg.data.action = TREASURE_REQUEST;
    msg.data.u.treasure_request.sender = company_id;
    msg.data.u.treasure_request.field = field;
    msg.data.u.treasure_request.worker = worker_num;

    int sndret = msgsnd(museum_queue_id, &msg, sizeof(msg.data), 0);
    if (sndret == 0) {
        return true;
    } else {
        SYSCHK(errno == EINTR);
        return false;
    }
}

// Transfers a collection to the museum
void send_collection(int artifact) {
    museum_msg_t msg;
    msg.type = MUSEUM_TYPE;
    msg.data.action = COLLECTION_TRANSFER;
    msg.data.u.collection_transfer.sender = company_id;
    msg.data.u.collection_transfer.artefact = artifact;

    int sndret = msgsnd(museum_queue_id, &msg, sizeof(msg.data), 0);
    SYSCHK(sndret == 0);
}


int extract_treasure(int treasure, int artefacts[ARTEFACTS_NUM]) {
    int num = 0;  // will never be greater than ARTEFACTS_NUM
    for (int i = 2; i <= company_max_artefact && treasure > 1; ++i) {
        while (treasure % i == 0) {
            artefacts[num] = i;
            ++num;
            treasure /= i;
            LOG("%ld extracted %d\n", company_id, i);
        }
    }
    return num;
}

// true if succeeded
bool send_artefacts(int field, int artefacts_num,
                    int artefacts[ARTEFACTS_NUM]) {
    museum_msg_t msg;
    assert(delegate > 0);
    msg.type = delegate;
    msg.data.action = ARTEFACTS;
    msg.data.u.artefacts.sender = company_id;
    for (int i = 0; i < ARTEFACTS_NUM; ++i) {
        msg.data.u.artefacts.artefacts[i] = artefacts[i];
    }
    msg.data.u.artefacts.artefacts_num = artefacts_num;
    msg.data.u.artefacts.field = field;

    int sndret = msgsnd(museum_queue_id, &msg, sizeof(msg.data), 0);
    if (sndret == 0) {
        return true;
    } else {
        SYSCHK(errno == EINTR);
        return false;
    }
}

// returns queue type number for given worker
static long get_queue_type(long worker_number) {
    return worker_number + ((long)company_id + 1) * MAX_LENGTH;
}

// the process of the work on a particular field
static void excavate(long number) {
    worker_state_t worker_state = ASK_FOR_TREASURE;
    long queue_type = get_queue_type(number);

    int artefacts[ARTEFACTS_NUM];
    int artefacts_num;
    int field = permission.start + number;

    while (worker_state != DONE) {
        if (worker_state == ASK_FOR_TREASURE) {
            bool asked = ask_for_treasure(queue_type, field);
            if (asked) {
                worker_state = WAIT_FOR_TREASURE;
            }
        }

        if (worker_state == SEND_ARTEFACTS) {
            if (send_artefacts(field, artefacts_num, artefacts)) {
                pthread_mutex_lock(&mutex);
                for (int i = 0; i < artefacts_num; ++i) {
                    int val = artefacts[i];
                    confirmed_artefacts[val]++;
                    if (confirmed_artefacts[val] == val) {
                        collection_kinds_num++;
                    }
                }
                worker_state = ASK_FOR_TREASURE;
                pthread_mutex_unlock(&mutex);
            }
        }

        if (!running) {
            return;
        }

        if (worker_state == WAIT_FOR_TREASURE) {
            bank_msg_t msg;
            int rcvret = msgrcv(bank_queue_id, &msg, sizeof(msg.data),
                                queue_type, 0);
            SYSCHK(rcvret > 0);

            switch (msg.data.action) {
            case TREASURE:
                LOG("Received treasure %d by %ld\n",
                     msg.data.u.treasure, company_id);
                artefacts_num = extract_treasure(msg.data.u.treasure,
                                                 artefacts);
                worker_state = SEND_ARTEFACTS;
                break;
            case TERRAIN_REJECTION:
                worker_state = DONE;

                pthread_mutex_lock(&mutex);
                permission.active_fields -= 1;
                if (permission.active_fields == 0) {
                    permission.depth = 0;
                    permission.start = -1;
                    asked_for_permission = false;
                }
                pthread_mutex_unlock(&mutex);
                break;
            default:
                assert(false);
            }
        }
    }
}

static void wake_employees() {
    pthread_mutex_lock(&mutex);
    for (int i = 0; i < company_workers_num; ++i) {
        workers[i].has_work = true;
        pthread_cond_signal(&workers[i].condition);
    }
    pthread_mutex_unlock(&mutex);
}

void *worker_main(void *arg) {
    long number = (long)arg;

    while (running) {
        pthread_mutex_lock(&mutex);
        while (!workers[number].has_work) {
            pthread_cond_wait(&workers[number].condition, &mutex);
        }
        workers[number].has_work = false;
        pthread_mutex_unlock(&mutex);

        if (!running) {
            // the program finished
            return NULL;
        }

        LOG("Employee (%ld, %ld) woken up.\n", company_id, number);

        excavate(number);
    }

    return NULL;
}

static void company_main_loop() {
    while (running) {
        if (collection_kinds_num > 0) {
            for (int i = 1; i <= company_max_artefact; ++i) {
                int collections_sent = 0;
                pthread_mutex_lock(&mutex);
                while (confirmed_artefacts[i] >= i) {
                    company_balance += i * 10;
                    confirmed_artefacts[i] -= i;
                    collections_sent += 1;
                    LOG("Company %ld selling one collection of type %d\n",
                        company_id, i);
                }
                collection_kinds_num -= 1;
                pthread_mutex_unlock(&mutex);
                for (int i = 0; i < collections_sent; ++i)
                    send_collection(i);
            }
        }

        pthread_mutex_lock(&mutex);
        if (permission.depth == 0 && !asked_for_permission &&
                company_balance > company_fixed_fee) {
            long long offer = company_balance;
            company_balance -= offer;  // reserve the amount
            pthread_mutex_unlock(&mutex);
            asked_for_permission = ask_for_permission(offer);
        } else {
            pthread_mutex_unlock(&mutex);
        }

        bank_msg_t msg;
        if (asked_for_permission) {
            int rcvret = msgrcv(bank_queue_id, &msg, sizeof(msg.data),
                                company_id, 0);
            if (rcvret == -1 && errno == EINTR) {
                continue;
            }
            SYSCHK(rcvret > 0);

            switch (msg.data.action) {
            case ENQUIRY_REJECTION:
                asked_for_permission = false;
                pthread_mutex_lock(&mutex);
                long long offer = msg.data.u.enquiry_rejection.offer;
                assert(offer >= company_fixed_fee);
                company_balance = company_balance + offer - company_fixed_fee;
                pthread_mutex_unlock(&mutex);
                break;
            case ENQUIRY_ACCEPTATION:
                // the money has been reserved, so the balance is correct now
                asked_for_permission = false;
                permission.depth = msg.data.u.enquiry_acceptation.depth;
                permission.start = msg.data.u.enquiry_acceptation.start;
                LOG("Company %ld has permission from %d\n",
                    company_id, permission.start);
                delegate = msg.data.u.enquiry_acceptation.delegate;
                permission.active_fields = company_workers_num;
                assert(delegate > 0);
                wake_employees();
                break;
            default:
                assert(false);
            }
        } else {
            // just to prevent flooding and loosing all the money so fast
            usleep(SLEEP_USECONDS);
        }

        pthread_mutex_lock(&mutex);
        if (permission.depth == 0 && company_balance < company_fixed_fee
                && collection_kinds_num == 0 && !asked_for_permission) {
            running = false;
        }
        pthread_mutex_unlock(&mutex);
    }
}

void set_up_queues() {
    bank_queue_id = msgget(BANK_KEY, 0700);  // created by the bank
    SYSCHK(bank_queue_id >= 0);

    museum_queue_id = msgget(MUSEUM_KEY, IPC_CREAT | 0700);
    SYSCHK(museum_queue_id >= 0);
}

void company_main(int id, long long balance, int workers_num,
                  long long fixed_fee, int max_artefact) {
    company_id = id;
    company_balance = balance;
    company_workers_num = workers_num;
    company_fixed_fee = fixed_fee;
    company_max_artefact = max_artefact;
    confirmed_artefacts = calloc(max_artefact, sizeof(int));
    workers = calloc(workers_num, sizeof(workers[0]));

    set_sigint_handler();
    set_up_queues();

    pthread_mutex_init(&mutex, NULL);

    for (long i = 0; i < workers_num; ++i) {
        pthread_cond_init(&workers[i].condition, NULL);
        pthread_create(&workers[i].thread, NULL, &worker_main, (void *)i);
    }

    company_main_loop();

    // wake up all threads, so that they can finish
    assert(!running);
    wake_employees();
    pthread_mutex_lock(&mutex);
    LOG("Cancelling threads in company %ld\n", company_id);
    for (int i = 0; i < workers_num; ++i) {
        SYSCHK(pthread_cancel(workers[i].thread) == 0);
    }
    pthread_mutex_unlock(&mutex);

    for (int i = 0; i < workers_num; ++i) {
        pthread_join(workers[i].thread, NULL);
        pthread_cond_destroy(&workers[i].condition);
    }

    pthread_mutex_destroy(&mutex);

    report();

    LOG("Company %ld exiting\n", company_id);

    free(confirmed_artefacts);
    free(workers);

    quit();
}
