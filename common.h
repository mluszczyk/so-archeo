#ifndef COMMON_H
#define COMMON_H

#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define SYSCHK(PRED) \
  if(!(PRED)) { \
    printf("SYSCHK fail in %s at line %d\n", __FILE__, __LINE__); \
    perror(#PRED); \
    exit(-1); \
}

#ifdef LOGGING
#define LOG(...) fprintf(stderr, __VA_ARGS__)
#else
#define LOG(...) ((void)0)
#endif

extern const int SLEEP_USECONDS;
extern const key_t BANK_KEY;
extern const long BANK_TYPE;

extern const key_t MUSEUM_KEY;
extern const long MUSEUM_TYPE;

#define MAX_LENGTH 1000000
#define MAX_COMPANY_NUM 1000001
#define REPORT_SIZE 500
// max number of artefacts on a single level of a field
#define ARTEFACTS_NUM 40

enum Action {
    COMPANY_SHUTDOWN,
    MUSEUM_SHUTDOWN,
    BANK_SHUTDOWN,
    COMPANY_REPORT,
    PERMISSION_ENQUIRY,
    ENQUIRY_REJECTION,
    ENQUIRY_ACCEPTATION,
    TREASURE_REQUEST,  // company asking for the number representing treasure
    TREASURE,  // sending symbol representing buried treasure to the company
    ARTEFACTS,  // sending list of artefacts
    TERRAIN_REJECTION,  // response prohibiting access to given terrain
    COLLECTION_TRANSFER // selling artefacts to the museum
};

typedef struct {
    enum Action action;
    int who;
    union {
        int treasure;  // number representing buried treasure
        struct {
            long long offer;  // amount of money offered
        } enquiry_rejection;
        struct {
            long long offer;
            int start;
            int depth;
            long delegate;
        } enquiry_acceptation;
    } u;
} bank_data_t;

typedef struct {
    long type;
    bank_data_t data;
} bank_msg_t;

typedef struct {
    enum Action action;
    union {
        struct {
            long sender;
            char report[REPORT_SIZE];
        } company_report;
        struct {
            long sender;
            int field;
            int artefacts[ARTEFACTS_NUM];
            int artefacts_num;
        } artefacts;
        struct {
            long sender;
            int field;
            long worker;  // queue for the response
        } treasure_request;
        struct {
            long sender;
            int artefact;
        } collection_transfer;
        struct {
            long sender;
            long long offer;
            int workers_num;
        } permission_enquiry;
    } u;
} museum_data_t;

typedef struct {
    long type;
    museum_data_t data;
} museum_msg_t;

#endif
